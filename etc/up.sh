#!/bin/bash
/usr/sbin/iptables -t mangle -I OUTPUT -m owner --uid-owner vpn -j MARK --set-mark 42
/usr/sbin/iptables -t mangle -I OUTPUT -d 192.168.1.0/24 -m owner --uid-owner vpn -j RETURN
/usr/sbin/iptables -t nat -I POSTROUTING -o tun0 -j MASQUERADE
/usr/sbin/ip rule add fwmark 42 table 42

for f in /proc/sys/net/ipv4/conf/*/rp_filter; do
    echo 0 > $f
    done;


TUN0_SUB=$(/usr/sbin/ip -f inet -o addr show tun0 | cut -d\  -f 9 | sed 's/\/32//')
TUN0_IP=$(/usr/sbin/ip -f inet -o addr show tun0 | cut -d\  -f 7 | cut -d/ -f 1)
/usr/sbin/ip route add default via $TUN0_IP table 42

/usr/sbin/ip route add 192.168.255.0/24 via $TUN0_SUB dev tun0