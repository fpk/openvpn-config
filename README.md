# OpenVPN config for single application

## Install
	useradd vpn
	cp etc/up.sh /etc/openvpn/
	cp etc/down.sh /etc/openvpn/
	cp systemd/openvpn.service /etc/systemd/system/openvpn@YOUR_USERNAME.service
	
## Install openvpn server from docker hub
	https://hub.docker.com/r/kylemanna/openvpn/
	
## Start openvpn service
	systemctl enable openvpn@your_username
	systemctl start openvpn@your_username
	
## How to run
	xhost -
	sudo -u vpn -H your program

